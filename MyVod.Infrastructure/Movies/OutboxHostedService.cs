using System;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MyVod.Domain.Movies;
using Newtonsoft.Json;

namespace MyVod.Infrastructure.Movies
{
    public class OutboxHostedService : BackgroundService
    {
        private int executionCount = 0;
        private readonly ILogger<OutboxHostedService> _logger;
        private readonly IServiceProvider _services;
        private Timer _timer;

        public OutboxHostedService(ILogger<OutboxHostedService> logger, IServiceProvider services)
        {
            _logger = logger;
            _services = services;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await Task.Delay(1000, stoppingToken);
                
                using var scope = _services.CreateScope();
                
                var context = scope.ServiceProvider.GetService<MoviesContext>();
                var mediator = scope.ServiceProvider.GetService<IMediator>();
                
                var messages = context.Outbox.ToList();
                
                foreach (var message in messages)
                {
                    var type = Assembly.GetAssembly(typeof(Movie)).GetType(message.Type);
                    var notification = JsonConvert.DeserializeObject(message.Data, type);
                    await mediator.Publish((INotification) notification);
                }
                
                context.Outbox.RemoveRange(messages);
                await context.SaveChangesAsync(stoppingToken);
            }
        }
    }
}