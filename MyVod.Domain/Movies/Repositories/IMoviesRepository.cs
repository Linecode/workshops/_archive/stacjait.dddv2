using System.Threading.Tasks;
using Linecode.DDD;

namespace MyVod.Domain.Movies.Repositories
{
    public interface IMoviesRepository
    {
        IUnitOfWork UnitOfWork { get; }
        Task<Movie> Get(MovieId id);
        void Add(Movie movie);
    }
}