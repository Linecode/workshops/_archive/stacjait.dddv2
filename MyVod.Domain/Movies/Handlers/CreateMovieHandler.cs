using System;
using System.Threading;
using System.Threading.Tasks;
using Linecode.DDD;
using MediatR;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using MyVod.Domain.Movies.Commands;
using MyVod.Domain.Movies.Repositories;

namespace MyVod.Domain.Movies.Handlers
{
    public class CreateMovieHandler : IRequestHandler<CreateMovieCommand, Result<MovieId>>
    {
        private readonly IMoviesRepository _repository;

        public CreateMovieHandler(IMoviesRepository repository)
        {
            _repository = repository;
        }

        public async Task<Result<MovieId>> Handle(CreateMovieCommand request, CancellationToken cancellationToken)
        {
            var movie = Movie.New(request.Title, request.Description);

            try
            {
                _repository.Add(movie);
                await _repository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
            }
            catch (Exception e)
            {
                return Result<MovieId>.Error(new Exception());
            }

            return Result<MovieId>.Success(movie.Id);
        }
    }
}