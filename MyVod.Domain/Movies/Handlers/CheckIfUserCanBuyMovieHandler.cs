using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Linecode.DDD;
using Linecode.DDD.Exceptions;
using MediatR;
using MyVod.Domain.Movies.Commands;
using MyVod.Domain.Movies.Policies.Buy;
using MyVod.Domain.Movies.Repositories;

namespace MyVod.Domain.Movies.Handlers
{
    public class CheckIfUserCanBuyMovieHandler : IRequestHandler<CheckIfUserCanBuyMovieCommand, Result<bool>>
    {
        private readonly IMoviesRepository _moviesRepository;

        public CheckIfUserCanBuyMovieHandler(IMoviesRepository moviesRepository)
        {
            _moviesRepository = moviesRepository;
        }
        
        public async Task<Result<bool>> Handle(CheckIfUserCanBuyMovieCommand request, CancellationToken cancellationToken)
        {
            var movie = await _moviesRepository.Get(request.MovieId);

            if (movie is null)
                return Result<bool>.Error(new NotFoundException());

            var policy = BuyPolicies.Default;
            
            if (request.User.Groups.Contains("Vip"))
                policy = BuyPolicies.Vip;

            var result = movie.CanUserBuy(request.User, policy);
            
            return Result<bool>.Success(result.IsSuccessful);
        }
    }
}