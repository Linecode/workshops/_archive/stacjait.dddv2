using System.Collections.Generic;
using System.Linq;

namespace Linecode.DDD.Extensions
{
    public static class EnumerableExtensions
    {
        public static bool NotContains<T>(this IEnumerable<T> list, T value)
            => !list.Contains(value);
    }
}